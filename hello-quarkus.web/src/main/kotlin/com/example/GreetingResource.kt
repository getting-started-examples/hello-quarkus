package com.example

import com.example.hello.quarkus.Clock
import com.example.hello.quarkus.system.SystemClock
import java.time.ZonedDateTime
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/hello")
class GreetingResource(private val service : MessageService, private val clock : Clock) {
    @GET()
    @Path("v1")
    @Produces(MediaType.APPLICATION_JSON)
    fun hello() = HelloResponse(service.prepareMessage(), clock.now)
}

@Path("/hello")
class GreetingResource2(private val service : MessageService, private val clock : Clock) {
    @GET()
    @Path("v2")
    @Produces(MediaType.APPLICATION_JSON)
    fun hello() = HelloResponse(service.prepareMessage() + " [2]", clock.now)
}


data class HelloResponse(val text: String, val now: ZonedDateTime)

interface MessageSource {
    fun getMessage() : String
}

class HardcodedMessageSource : MessageSource {
    override fun getMessage(): String {
        return "Hello, World!"
    }
}

interface MessageService {
    fun prepareMessage() : String
}

class UpperCasingMessageService(private val source: MessageSource) : MessageService {
    override fun prepareMessage(): String {
        return source.getMessage().uppercase()
    }
}

class BeanProducer {
    @ApplicationScoped
    fun messageSource(): MessageSource {
        return HardcodedMessageSource()
    }

    @ApplicationScoped
    fun messageService(source: MessageSource): MessageService {
        return UpperCasingMessageService(source)
    }

    @ApplicationScoped
    fun clock() : Clock {
        return SystemClock()
    }
}
