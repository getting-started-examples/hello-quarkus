package com.example.hello.quarkus.system

import com.example.hello.quarkus.Clock
import java.time.ZonedDateTime

class SystemClock : Clock {
    override val now: ZonedDateTime
        get() = ZonedDateTime.now()
}