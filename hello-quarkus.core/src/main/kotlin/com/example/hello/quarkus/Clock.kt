package com.example.hello.quarkus

import java.time.ZonedDateTime

interface Clock {
    val now : ZonedDateTime
}